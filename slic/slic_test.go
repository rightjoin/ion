package slic

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestContains(t *testing.T) {

	var arr = []interface{}{"one", "11", 1}

	assert.True(t, Contains(arr, "11"), "'11' should be found")
	assert.True(t, Contains(arr, 1), "1 should be found")

	assert.False(t, Contains(arr, 11), "11 should not be found")
	assert.False(t, Contains(arr, "1"), "'1' should not be found")
}
