package slic

import "reflect"
import "fmt"

// FindOne returns the first item that has the given field, and given
// value for that field. Returns nil otherwise
func FindOne(slice interface{}, fldName string, fldValue interface{}) interface{} {
	switch reflect.TypeOf(slice).Kind() {
	case reflect.Slice:
		v := reflect.ValueOf(slice)
		for i := 0; i < v.Len(); i++ {
			fld := v.Index(i).FieldByName(fldName)
			if fld.IsValid() && fld.Interface() == fldValue {
				return v.Index(i).Interface()
			}
		}
	default:
		panic("slice expected")
	}

	return nil
}

// Find returns all the matching items of a slice where the Field name
// has the given value
func Find(slice interface{}, fldName string, fldValue interface{}) []interface{} {

	var out = []interface{}{}

	switch reflect.TypeOf(slice).Kind() {
	case reflect.Slice:
		v := reflect.ValueOf(slice)
		for i := 0; i < v.Len(); i++ {
			fld := v.Index(i).FieldByName(fldName)
			if fld.IsValid() && fld.Interface() == fldValue {
				out = append(out, v.Index(i).Interface())
			}
		}
	default:
		panic("slice expected")
	}

	return out
}

// Extract fetches the given field value from
// all the items of the slice.
func Extract(slice interface{}, fldName string) []interface{} {
	var out = []interface{}{}

	switch reflect.TypeOf(slice).Kind() {
	case reflect.Slice:
		v := reflect.ValueOf(slice)
		for i := 0; i < v.Len(); i++ {
			field := v.Index(i).FieldByName(fldName)
			if field.IsValid() {
				out = append(out, field.Interface())
			}
		}
	default:
		return nil
	}

	return out
}

// Group groups the given slice into chunks as per the values
// of the given field name
func Group(slice interface{}, fldName string) map[string][]interface{} {
	var out = map[string][]interface{}{}

	switch reflect.TypeOf(slice).Kind() {
	case reflect.Slice:
		v := reflect.ValueOf(slice)
		for i := 0; i < v.Len(); i++ {
			field := v.Index(i).FieldByName(fldName)
			if field.IsValid() {
				key := fmt.Sprint(field.Interface())
				val := v.Index(i).Interface()
				if _, ok := out[key]; !ok {
					out[key] = []interface{}{val}
				} else {
					out[key] = append(out[key], val)
				}
			}
		}
	default:
		return nil
	}

	return out
}

func ContainsString(haystack []string, needle string) bool {
	for i := range haystack {
		if haystack[i] == needle {
			return true
		}
	}
	return false
}

func Contains(haystack []interface{}, needle interface{}) bool {
	for i := range haystack {
		if haystack[i] == needle {
			return true
		}
	}
	return false
}
