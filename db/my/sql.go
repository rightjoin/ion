package my

import "github.com/jinzhu/gorm"

var DefaultDB *gorm.DB

func sqlHasRows(sql string) bool {
	rows, err := DefaultDB.Raw(sql).Rows()
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	return rows.Next()
}

func sqlExec(sql string) {
	err := DefaultDB.Exec(sql).Error
	if err != nil {
		panic(err)
	}
}
