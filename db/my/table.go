package my

import (
	"fmt"
	"reflect"
	"strings"

	"bitbucket.org/rightjoin/ion/conv"
)

// TableName return the table name of a given gorm model.
// It checks whetehr the method TableName has been used
// or not. It then retreives the appropriate table name.
// This function accepts both, model and a its pointer
func TableName(m interface{}) string {
	t := reflect.TypeOf(m)
	v := reflect.ValueOf(m)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
		v = v.Elem()
	}

	_, ok := t.MethodByName("TableName")
	if ok {
		name := v.MethodByName("TableName").Call([]reflect.Value{})
		return name[0].String()
	}

	return conv.CaseSnake(t.Name())
}

type Table struct {
	Name string
}

func NewTable(name string) *Table {
	return &Table{
		Name: name,
	}
}

func NewTableFromModel(model interface{}) *Table {
	return &Table{
		Name: TableName(model),
	}
}

// Exists checks if the table is present in
// the database or not
func (t *Table) Exists() bool {
	sql := fmt.Sprintf("show tables like '%s'", t.Name)
	return sqlHasRows(sql)
}

func (t *Table) ContainsData() bool {
	sql := fmt.Sprintf("select * from %s limit 1", t.Name)
	return sqlHasRows(sql)
}

func (t *Table) Drop(force bool) bool {

	if !t.Exists() {
		return true
	}

	if t.ContainsData() && force == false {
		return false
	}

	sql := fmt.Sprintf("drop table %s", t.Name)
	sqlExec(sql)

	return true
}

func (t *Table) GetFields() []Field {
	var fields []Field
	sql := fmt.Sprintf("desc %s", t.Name)
	err := DefaultDB.Raw(sql).Find(&fields).Error
	if err != nil {
		panic(err)
	}
	return fields
}

func (t *Table) GetField(field string) *Field {
	fields := t.GetFields()
	for _, f := range fields {
		if f.Name == field {
			return &f
		}
	}
	return nil
}

func (t *Table) GetAutoIncrField() *Field {
	flds := t.GetFields()
	for _, f := range flds {
		if strings.Contains(f.Extra, "auto_increment") {
			return &f
		}
	}
	return nil
}

func (t *Table) GetPrimaryKeys() []Field {
	pkeys := []Field{}

	flds := t.GetFields()
	for _, f := range flds {
		if strings.Contains(f.Key, "PRI") {
			pkeys = append(pkeys, f)
		}
	}
	return pkeys
}
