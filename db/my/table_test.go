package my

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

type ModelYo struct {
}

type ModelZo struct {
}

func (m ModelZo) TableName() string {
	return "zo_model"
}

func TestTableName(t *testing.T) {
	Convey("Given a two models", t, func() {
		Convey("Table names via struct and address should match", func() {
			So(TableName(ModelYo{}), ShouldEqual, "model_yo")
			So(TableName(&ModelYo{}), ShouldEqual, "model_yo")
			So(TableName(ModelZo{}), ShouldEqual, "zo_model")
			So(TableName(&ModelZo{}), ShouldEqual, "zo_model")
		})

	})
}
