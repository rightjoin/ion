package my

import "fmt"

type Field struct {
	Name    string  `gorm:"column:Field"`
	Type    string  `gorm:"column:Type"`
	Null    string  `gorm:"column:Null"`
	Key     string  `gorm:"column:Key"`
	Default *string `gorm:"column:Default"`
	Extra   string  `gorm:"column:Extra"`
}

func (f *Field) GetInfo() string {
	key := fmt.Sprintf("%s %s", f.Name, f.Type)
	if f.Null == "NO" {
		key += " NOT NULL"
	}
	if f.Default != nil {
		key += " DEFAULT " + *(f.Default)
	}
	key += " " + f.Extra
	return key
}
