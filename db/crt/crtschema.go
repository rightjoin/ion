package crt

import "github.com/jmoiron/sqlx"

var Connection string
var dbc *sqlx.DB

func Build(ns string, models ...interface{}) {
	if Connection == "" {
		panic("Store.Conn is empty")
	}

	var err error
	dbc, err = sqlx.Open("crate", Connection)
	if err != nil {
		panic(err)
	}

	for _, addr := range models {
		tbl := NewTable2(addr)
		tbl.ns = ns

		if tbl.exists() && tbl.hasData() {
			panic("table has data, can't delete: " + tbl.name)
		}

		tbl.drop(false)

		tbl.create()
	}
}
