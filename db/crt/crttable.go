package crt

import (
	_ "database/sql"
	"fmt"
	"reflect"

	"bitbucket.org/rightjoin/ion/conv"
	"bitbucket.org/rightjoin/ion/refl"
	_ "github.com/herenow/go-crate"
)

type table struct {
	ns    string
	name  string
	addr  interface{}
	model interface{}
}

func NewTable(tbl string) table {
	return table{name: tbl}
}

func NewTable2(maddr interface{}) *table {
	typ := reflect.TypeOf(maddr)
	if typ.Elem() == nil {
		panic("model must be an address")
	}

	return &table{
		name:  conv.CaseSnake(typ.Elem().Name()),
		addr:  maddr,
		model: reflect.ValueOf(maddr).Elem().Interface(),
	}
}

func (t *table) exists() bool {
	sql := fmt.Sprintf("show tables like '%s.%s'", t.ns, t.name)
	return sqlHasRows(sql)
}

func (t *table) hasData() bool {
	sql := fmt.Sprintf("select * from %s.%s limit 1", t.ns, t.name)
	return sqlHasRows(sql)
}

func (t *table) drop(force bool) {

	if !t.exists() {
		return
	}

	if t.hasData() && force == false {
		return
	}

	sql := fmt.Sprintf("drop table %s.%s", t.name)
	sqlExec(sql)
}

func (t *table) create() {
	sql := fmt.Sprintf("create table %s.%s (\n", t.ns, t.name)
	flds := refl.NestedFields(t.model)
	for i, f := range flds {
		fname := conv.CaseSnake(f.Name)
		fmt.Println(
			"Name:", f.Name,
			"Type:", f.Type.Name(),
			"Signature:", refl.TypeSignature(f.Type),
		)
		sign := refl.TypeSignature(f.Type)
		switch sign {
		case "string", "*string":
			sql += fname + " string"
		case "uint8":
			sql += fname + " byte"
		case "uint16", "int16":
			sql += fname + " short"
		case "uint", "int", "int32":
			sql += fname + " integer"
		case "uint64", "int64":
			sql += fname + " long"
		case "*sl:.", "sl:.":
			sql += fname + " array"
		case "*sl:.string", "sl:.string":
			sql += fname + " array(string)"
		case "*map", "map":
			sql += fname + " object"
		case "*st:time.Time", "st:time.Time":
			sql += fname + " datetime"
		default:
			panic("type not understood:" + sign + " for:" + f.Name)
		}
		if i < len(flds)-1 {
			sql += ",\n"
		}
	}

	sql += ")"
	fmt.Println(sql)
	sqlExec(sql)
}

func sqlHasRows(sql string) bool {
	rows, err := dbc.Query(sql)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	return rows.Next()
}

func sqlExec(sql string) {
	_, err := dbc.Exec(sql)
	if err != nil {
		panic(err)
	}
}
