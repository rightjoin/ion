package db

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestJArrContains(t *testing.T) {

	Convey("Given an array of ints", t, func() {
		arr := NewJArr(1, 2, 3, 4, 5)
		Convey("Contains should work properly", func() {
			So(arr.Contains(3), ShouldBeTrue)
			So(arr.Contains(8), ShouldBeFalse)
		})
	})

}
