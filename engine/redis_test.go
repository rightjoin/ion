package engine

import (
	"math/rand"
	"testing"

	"fmt"

	"time"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/assert"
)

var redis_host string = "localhost"
var redis_port int = 6379
var redis_db int = 0
var redis_que string = "abcdef"

func TestRedisGetSet(t *testing.T) {

	r := NewRedis(redis_host, redis_port, redis_db)

	key := fmt.Sprintf("key:%0.6f", rand.Float64())
	val := fmt.Sprintf("val:%0.6f", rand.Float64())

	Convey("Given a Redis implementation", t, func() {

		Convey("When a key is NOT set", func() {
			Convey("Then get-operation should give nil value and a proper error", func() {
				v, err := r.Get(key)
				So(err, ShouldNotBeNil)
				So(v, ShouldBeNil)
			})
		})

		Convey("When key is set", func() {
			r.Set(key, []byte(val), time.Second*5)
			Convey("Then get-operation should give proper value and nil error", func() {
				v, err := r.Get(key)
				So(err, ShouldBeNil)
				So(string(v), ShouldEqual, val)
			})
		})
	})
}

func TestRedis_QuePushPop(t *testing.T) {

	emptyTheQue()
	r := NewRedis2(redis_host, redis_port, redis_db, redis_que)

	Convey("Given an empty Redis queue", t, func() {

		Convey("Pop should produce error", func() {
			b, err := r.Pop()
			So(b, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("PopWait should produce error", func() {
			b, err := r.PopWait(time.Second * 1)
			So(b, ShouldBeNil)
			So(err, ShouldNotBeNil)
		})

		Convey("When you push something", func() {
			Convey("Then PopWait should return the value", func() {
				r.Push([]byte("zero1"))
				b, err := r.PopWait(time.Second * 1)
				So(string(b), ShouldEqual, "zero1")
				So(err, ShouldBeNil)
			})
			Convey("Then Pop should return the value", func() {
				r.Push([]byte("zero2"))
				b, err := r.Pop()
				So(string(b), ShouldEqual, "zero2")
				So(err, ShouldBeNil)
			})
		})

		Convey("When you push two values", func() {
			r.Push([]byte("one"))
			r.Push([]byte("two"))
			Convey("Then the lenght must be 2", func() {
				i, err := r.Len()
				So(err, ShouldBeNil)
				So(i, ShouldEqual, 2)
				Convey("And when you pop one value", func() {
					v, e1 := r.Pop()
					Convey("Then value should match and length must be 1", func() {
						So(e1, ShouldBeNil)
						So(string(v), ShouldEqual, "one")
						l, e2 := r.Len()
						So(e2, ShouldBeNil)
						So(l, ShouldEqual, 1)
						Convey("And when you pop another value", func() {
							v2, e3 := r.Pop()
							Convey("Then the value must match and length must be 0", func() {
								So(e3, ShouldBeNil)
								So(string(v2), ShouldEqual, "two")
								l2, e4 := r.Len()
								So(e4, ShouldBeNil)
								So(l2, ShouldEqual, 0)
							})
						})
					})
				})
			})
		})
	})

}

func emptyTheQue() {
	r := NewRedis2(redis_host, redis_port, redis_db, redis_que)
	for {
		i, err := r.Len()
		if err != nil {
			panic(err)
		}
		if i != 0 {
			r.Pop()
		} else {
			break
		}
	}
	r.Close()
}

func TestRedisMegaReadWrites(t *testing.T) {

	// seed the random number generator
	rand.Seed(time.Now().UTC().UnixNano())

	// create an array and populate it with random values
	var nums = make([]int, 1000)
	for i := range nums {
		nums[i] = rand.Int()
	}

	// create queue with random name
	name := fmt.Sprintf("queue-test-%d", rand.Int())
	q := NewRedis2(redis_host, redis_port, redis_db, name)

	// push values into the queue
	go func() {
		for _, n := range nums {
			q.Push([]byte(fmt.Sprintf("%d", n)))
		}
	}()

	// match queue entries with random number array
	for _, n := range nums {
		b, err := q.PopWait(1 * time.Second)
		time.Sleep(50 * time.Millisecond)
		assert.Nil(t, err)
		assert.Equal(t, string(b), fmt.Sprintf("%d", n))
	}

}
