package ion

import "fmt"

type M map[string]interface{}

func (me *M) Uint(key string) (uint, error) {

	val, found := (*me)[key]
	if !found {
		return 0, fmt.Errorf("key: %s not found", key)
	}

	switch val.(type) {
	case int:
		return uint(val.(int)), nil
	case uint:
		return val.(uint), nil
	default:
		return 0, fmt.Errorf("key: %s is not uint", key)
	}
}

func (me *M) UintOr(key string, deflt uint) uint {

	val, err := me.Uint(key)

	if err == nil {
		return val
	}
	return deflt
}
