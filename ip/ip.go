package ip

import (
	"fmt"
	"net"
)

// GetLocal returns the non loopback local IP of the host
func GetLocal() string {
	// http://stackoverflow.com/questions/23558425/how-do-i-get-the-local-ip-address-in-go
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return ""
	}
	for _, address := range addrs {
		// check the address type and if it is not a loopback the display it
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return ""
}

// HostHasIP returns true if any of the networking interfaces on this
// device match the given ip address. In other words, it return true
// if the given ip belongs to the current machine
func HostHasIP(ipStr string) bool {
	ifaces, _ := net.Interfaces()
	for _, i := range ifaces {
		addrs, _ := i.Addrs()
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			if ipStr == fmt.Sprint(ip) {
				return true
			}
		}
	}

	return false
}
