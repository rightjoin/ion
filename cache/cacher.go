package cache

import (
	"fmt"
	"strings"
	"time"

	"bitbucket.org/rightjoin/ion/conf"
	"bitbucket.org/rightjoin/ion/db/cstr"
	"bitbucket.org/rightjoin/ion/engine"
	"bitbucket.org/rightjoin/ion/key"
)

type Cacher interface {
	key.KeyFormatter
	Set(key string, data []byte, expireIn time.Duration)
	Get(key string) ([]byte, error)
	Delete(key string) error
	Close()
}

// This variable stores the many inproc memory
// stores created under different parents.
// If a process refers to same parent at another location
// then reuse the inproc object (so that gets can follow sets)
var inprocs = make(map[string]Cacher)

func NewCacher(container ...string) (out Cacher) {
	parent := strings.Join(container, ".")

	engn := conf.String("", parent, "engine")
	if engn == "" {
		panic("cache engine is not specified")
	}

	switch engn {
	case "memcache":
		{
			cnf := cstr.Memcache{}
			conf.Struct(&cnf, parent)
			out = engine.NewMemcache(cnf.Host, cnf.Port)
		}

	case "inproc", "inproccache":
		{
			var ok bool

			// if any inproc exists, then reuse it
			if out, ok = inprocs[parent]; !ok {
				expiry := conf.String("1h", parent, "lifetime")
				life, err := time.ParseDuration(expiry)
				if err != nil {
					panic(err)
				}
				out = engine.NewInProcCache(life)
				inprocs[parent] = out
			}
		}

	case "log":
		{
			dir := conf.String("", parent, "dir")
			inner := NewCacher(parent, "inner")
			out = NewCacheLogger(dir, inner)
		}

	case "redis":
		{
			cnf := cstr.Redis{}
			conf.Struct(&cnf, parent)
			out = engine.NewRedis(cnf.Host, cnf.Port, cnf.Db)
		}

	default:
		panic(fmt.Errorf("Unknown cache engine: %s", engn))
	}

	return out
}
