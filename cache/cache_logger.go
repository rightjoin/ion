package cache

import (
	"os"
	"strings"
	"time"

	"bitbucket.org/rightjoin/ion/key"
	log "github.com/rightjoin/log15"
)

type CacheLogger struct {
	key.AsIsFormat
	Inner Cacher
}

func NewCacheLogger(dir string, inner Cacher) Cacher {

	if dir == "" {
		dir, _ = os.Getwd()
	}

	if !strings.HasSuffix(dir, "/") && !strings.HasSuffix(dir, "\\") {
		dir += "/"
	}

	return CacheLogger{
		Inner: inner,
	}
}

func (c CacheLogger) Set(key string, data []byte, expireIn time.Duration) {
	k := c.Format(key)
	c.Inner.Set(key, data, expireIn)
	log.Info("cache set", "ref", "cache", "key-orig", k, "key-final", c.Inner.Format(key), "data", data)
}

func (c CacheLogger) Get(key string) ([]byte, error) {
	data, err := c.Inner.Get(key)
	k := c.Format(key)

	if err != nil {
		log.Info("cache get", "ref", "cache", "key-orig", k, "key-final", c.Inner.Format(key), "data", "<not-found>")
		return nil, err
	} else {
		log.Info("cache get", "ref", "cache", "key-orig", k, "key-final", c.Inner.Format(key), "data", data)
		return data, nil
	}
}

func (c CacheLogger) Delete(key string) error {
	k := c.Format(key)
	log.Info("cache delete", "ref", "cache", "key-orig", k, "key-final", c.Inner.Format(key))
	return c.Inner.Delete(key)
}

func (c CacheLogger) Close() {
	c.Inner.Close()
	log.Info("cache close", "ref", "cache")
}
