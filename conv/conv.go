package conv

import (
	"encoding/json"
	"strconv"
)

func String2Uint(inp string) uint {
	u, err := strconv.ParseUint(inp, 10, 64)
	if err != nil {
		panic(err)
	}
	return uint(u)
}

func ToBytes(obj interface{}) ([]byte, error) {
	return json.Marshal(obj)
}
